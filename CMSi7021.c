/*
 * CMSi7021.c
 *
  * Created: 13.02.2017 19:39:50
  *  Author: Christian M�llers
 */ 

#include <avr/io.h>
#include "CMSi7021.h"
#include "../CMGlobal.h"
#include <avr/interrupt.h>
#include <util/delay.h>
#include "../CMTwi/CMTwi.h"

/**
	@brief	none
	@param 	none
	@return	none
*/
uint16_t readFromSi7021Sensor(uint8_t regSi7021)
{
	uint8_t valueLSB = 0;
	uint8_t valueMSB = 0;
	uint8_t valueCRC = 0;
	uint16_t value = 0;
	
	i2cInit();
	i2cStart(Si7021Adress | twWrite);
	i2cSend(regSi7021);
	i2cStop();
	i2cStart(Si7021Adress | twRead);
	while((PORTC & (1<<PC5)));
	valueMSB = i2cReceiveAck();
	valueLSB = i2cReceiveAck();
	valueCRC = i2cReceiveNack();
	i2cStop();
	
	value = valueMSB << 8 | valueLSB;
	
	return value;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void softResetSi7021Sensor(void)
{
	i2cInit();
	i2cStart(Si7021Adress | twWrite);
	i2cSend(si7021SoftReset);
	_delay_ms(15);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getSi7021UserRegister(void)
{
	uint8_t value = 0;
	
	i2cInit();
	i2cStart(Si7021Adress | twWrite);
	i2cSend(readUserRegister);
	i2cStop();
	i2cStart(Si7021Adress | twRead);
	value = i2cReceiveNack();
	i2cStop();
	
	return value;
}


/**
	@brief	none
	@param 	none
	@return	none
*/
void setSi7021UserRegister(uint8_t regSi7021)
{
	uint8_t valueLSB = 0;
	uint8_t valueMSB = 0;
	uint8_t valueCRC = 0;
	uint8_t value = 0;
	
	i2cInit();
	i2cStart(Si7021Adress | twWrite);
	i2cSend(readUserRegister);
	i2cStop();
	i2cStart(Si7021Adress | twWrite);
	i2cSend(value);
	valueMSB = i2cReceiveAck();
	valueLSB = i2cReceiveAck();
	valueCRC = i2cReceiveNack();
	i2cStop();
	
	value = valueMSB << 8 | valueLSB;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
float readSi7021TempSensor(void)
{
	long stemp = 0;
	float temp = 0;
	
	stemp = readFromSi7021Sensor(tempMeasTriggerOne);
	
	// look into datasheet == site 14.
	stemp = ((long) ((17572 * stemp)>>16) - 4686);
	temp = stemp;
	temp = temp / 100;
	
	return temp;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
float readSi7021HumiditySensor(void)
{
	long shumidity = 0;
	float humidity = 0.0;
	
	shumidity = readFromSi7021Sensor(humidMeasTriggerOne);
	
	// look into datasheet == site 15.
	shumidity = (((long) (125 * shumidity >> 16) - 6));
	humidity = (float) shumidity;
	
	return humidity;
}