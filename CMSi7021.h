/*
 * CMSi7021.h
 *
 * Created: 13.02.2017 19:39:50
 *  Author: Christian M�llers
 */ 

#ifndef CMLM75_H_
#define CMLM75_H_

#define twWrite					0
#define twRead					1

#define Si7021Adress			0x80
#define si7021SoftReset			0xFE

#define tempMeasTriggerOne		0xE3 
#define humidMeasTriggerOne		0xE5 
#define tempMeasTriggerSec		0xF3
#define humiMeasTriggerSec		0xF5
#define writeUserRegister		0xE6
#define readUserRegister		0xE7

#define managementResolution	0x00
//	Bit 7 Bit 0 RH Temp
//	0 0 12 bits 14 bits
//	0 1 8 bits 12 bits
//	1 0 10 bits 13 bits
//	1 1 11 bits 11 bits

uint16_t readFromSi7021Sensor(uint8_t regSi7021);
float readSi7021TempSensor(void);
float readSi7021HumiditySensor(void);
void softResetSi7021Sensor(void);
uint8_t getSi7021UserRegister(void);
void setSi7021UserRegister(uint8_t regSi7021);

#endif /* CMLM75_H_ */